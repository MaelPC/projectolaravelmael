<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Llista de categories</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<x-app-layout>
    <x-slot name="header">
        <div class="d-flex flex-row align-items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Llista de categories') }}
            </h2>
        </div>
    </x-slot>

    <div class="container py-4">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Iva Percentage</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->iva }}</td>
                        <td>
                            <a href="{{ route('categories.editcategory', ['category' => $category->id]) }}" class="btn btn-success btn-sm">Edit</a>
                            <a href="{{ route('category.deleteconfirmation', ['category' => $category->id]) }}" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <a class="btn btn-primary" href={{route("categories.form")}}>
                Formulari per inserció de Categories
            </a>
        </div>
    </div>
</x-app-layout>

<script defer src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXlY3/3vzpFfgccz7zU3YLQjSxsb6N0IFB+z7rrn3A88mTAA8bT8b4O2L8w4" crossorigin="anonymous"></script>
<script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhG8c3AnjE7e3hf9gzUqvQhtP6HfhZfp26d5URjo8U2tE3JfCFk3PB6h59ni" crossorigin="anonymous"></script>
</body>
</html>
