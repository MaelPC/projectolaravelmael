<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Formulari Productes</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<x-app-layout>
    <x-slot name="header">
        <div class="d-flex flex-row align-items-center">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Crear Producte') }}
        </h2>
        </div>
    </x-slot>

    <div class="container py-4">
        <form action="{{ route('products.insert') }}" method="post">
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Nom</label>
                <input type="text" class="form-control" name="name" id="name" value="{{old("name")}}">
                @error('name')
                <div class="alert alert-danger mt-2">
                    Error en el nom, té més de 100 caràcters o ja existeix
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="quantity" class="form-label">Quantitat</label>
                <input type="number" class="form-control" name="quantity" id="quantity" value="{{old("quantity")}}">
                @error('quantity')
                <div class="alert alert-danger mt-2">
                    Error en la quantitat, aquest camp és obligatori
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="price" class="form-label">Preu</label>
                <input type="number" class="form-control" name="price" id="price" value="{{old("price")}}">
                @error('price')
                <div class="alert alert-danger mt-2">
                    Error en el preu, aquest camp és obligatori
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="description" class="form-label">Descripció</label>
                <textarea class="form-control" name="description" id="description">{{old("description")}}</textarea>
                @error('description')
                <div class="alert alert-danger mt-2">
                    Error en la descripció, aquest camp ha de tenir menys de 1000 caràcters
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="category_id" class="form-label">Categoria</label>
                <select name="category_id" id="category_id" class="form-select" required>
                    <option value="">Seleccioneu una categoria</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="discount_id" class="form-label">Descompte</label>
                <select name="discount_id" id="discount_id" class="form-select" required>
                    <option value="">Seleccioneu un descompte</option>
                    @foreach($discounts as $discount)
                        <option value="{{ $discount->id }}">{{ $discount->name }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
    </div>
</x-app-layout>

<script defer src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXlY3/3vzpFfgccz7zU3YLQjSxsb6N0IFB+z7rrn3A88mTAA8bT8b4O2L8w4" crossorigin="anonymous"></script>
<script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhG8c3AnjE7e3hf9gzUqvQhtP6HfhZfp26d5URjo8U2tE3JfCFk3PB6h59ni" crossorigin="anonymous"></script>
</body>
</html>
