<!doctype html>
<html lang="ca">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Formulari Clients</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<x-app-layout>
    <x-slot name="header">
        <div class="d-flex flex-row align-items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Crear Comanda') }}
            </h2>
        </div>
    </x-slot>

    <div class="container py-4">
        <form method="POST" action="{{ route('orders.store') }}">
            @csrf
            <div class="mb-3">
                <label for="client_id" class="form-label">Client</label>
                <select name="client_id" id="client_id" class="form-select" required>
                    <option value="">Seleccioneu un client</option>
                    @foreach($clients as $client)
                        <option value="{{ $client->id }}">{{ $client->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="order_date" class="form-label">Data de la Comanda</label>
                <input type="date" name="order_date" id="order_date" class="form-control" required>
            </div>
            <div id="products-container">
                <div class="product-entry mb-3">
                    <label for="products[0][id]" class="form-label">Producte</label>
                    <select name="products[0][id]" id="products[0][id]" class="form-select" required>
                        <option value="">Seleccioneu un producte</option>
                        @foreach($products as $product)
                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                        @endforeach
                    </select>
                    <label for="products[0][quantity]" class="form-label mt-2">Quantitat</label>
                    <input type="number" name="products[0][quantity]" id="products[0][quantity]" class="form-control" min="1" required >
                    @if(session('quantity'))
                        <div class="alert alert-danger mt-3">
                            {{ session('quantity') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="d-flex justify-content-center mt-4">
                <button type="button" id="add-product" class="btn btn-secondary me-2">Afegir Producte</button>
                <button type="submit" class="btn btn-primary">Fer una Comanda</button>
            </div>
        </form>
    </div>
</x-app-layout>

<script defer src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXlY3/3vzpFfgccz7zU3YLQjSxsb6N0IFB+z7rrn3A88mTAA8bT8b4O2L8w4" crossorigin="anonymous"></script>
<script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-cVKIPhG8c3AnjE7e3hf9gzUqvQhtP6HfhZfp26d5URjo8U2tE3JfCFk3PB6h59ni" crossorigin="anonymous"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        const addProductButton = document.getElementById('add-product');
        const productsContainer = document.getElementById('products-container');
        let productIndex = 1;

        addProductButton.addEventListener('click', function() {
            const productEntry = document.createElement('div');
            productEntry.className = 'product-entry mb-3';

            productEntry.innerHTML = `
                <label class="form-label" for="products[${productIndex}][id]">Producte</label>
                <select name="products[${productIndex}][id]" class="form-select">
                    <option value="">Seleccioneu un producte</option>
                    @foreach($products as $product)
            <option value="{{ $product->id }}">{{ $product->name }} </option>
                    @endforeach
            </select>
            <label class="form-label mt-2" for="products[${productIndex}][quantity]">Quantitat</label>
                <input type="number" name="products[${productIndex}][quantity]" class="form-control" placeholder="Quantitat" min="1" required>
            `;

            productsContainer.appendChild(productEntry);
            productIndex++;
        });
    });
</script>
</body>
</html>
