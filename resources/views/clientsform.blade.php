<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Formulari Clients</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<x-app-layout>
    <x-slot name="header">
        <div class="d-flex flex-row align-items-center">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Crear Client') }}
        </h2>
        </div>
    </x-slot>

    <div class="container py-4">
        <form action="{{ route('clients.insert') }}" method="post">
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Nom</label>
                <input type="text" class="form-control" name="name" id="name" value="{{old("name")}}">
                @error('name')
                <div class="alert alert-danger mt-2">
                    Error en el nom, té més de 100 caràcters o ja existeix
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="surname" class="form-label">Surname</label>
                <input type="text" class="form-control" name="surname" id="surname" value="{{old("surname")}}">
                @error('surname')
                <div class="alert alert-danger mt-2">
                    Error en el cognom/surname, té més de 100 caràcters.
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="text" class="form-control" name="email" id="email" value="{{old("email")}}">
                @error('email')
                <div class="alert alert-danger mt-2">
                    Error en el email, té més de 100 caràcters, ja existeix o no has introduit ningun email.
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="phonenumber" class="form-label">Phone Number</label>
                <input type="text" class="form-control" name="phonenumber" id="phonenumber" value="{{old("phonenumber")}}">
                @error('phonenumber')
                <div class="alert alert-danger mt-2">
                    Error en el Numero de telèfon/Phone Number,Error en el email, té més de 100 caràcters, ja existeix o no has introduit ningun email.
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="postalcode" class="form-label">Postal Code</label>
                <input type="text" class="form-control" name="postalcode" id="postalcode" value="{{old("postalcode")}}">
                @error('postalcode')
                <div class="alert alert-danger mt-2">
                    Error en el Codi Postal / Postal Code, té més de 20 caràcters o ja existeix.
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="address" class="form-label">Address</label>
                <input type="text" class="form-control" name="address" id="address" value="{{old("address")}}">
                @error('address')
                <div class="alert alert-danger mt-2">
                    Error en la direcció/Address, té més de 100 caràcters o ja existeix.
                </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
    </div>
</x-app-layout>

<script defer src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXlY3/3vzpFfgccz7zU3YLQjSxsb6N0IFB+z7rrn3A88mTAA8bT8b4O2L8w4" crossorigin="anonymous"></script>
<script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhG8c3AnjE7e3hf9gzUqvQhtP6HfhZfp26d5URjo8U2tE3JfCFk3PB6h59ni" crossorigin="anonymous"></script>
</body>
</html>
