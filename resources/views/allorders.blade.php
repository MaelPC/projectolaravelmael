<!doctype html>
<html lang="ca">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Totes les comandes</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>

<x-app-layout>
    <x-slot name="header">
        <div class="d-flex flex-row align-items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Llista de Comandes') }}
            </h2>
        </div>
    </x-slot>

    <div class="container py-4">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Client</th>
                    <th>Productes</th>
                    <th>Quantitat</th>
                    <th>Preu Unitari</th>
                    <th>Preu Total</th>
                    <th>Preu Total amb IVA</th>
                    <th>Data de la Comanda</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($orders as $order)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->client->name }}</td>
                        <td>
                            <ul class="list-unstyled">
                                @foreach ($order->products as $product)
                                    <li>{{ $product->name }}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>
                            <ul class="list-unstyled">
                                @foreach ($order->products as $product)
                                    <li>{{ $product->pivot->quantity }}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>
                            <ul class="list-unstyled">
                                @foreach ($order->products as $product)
                                    <li>{{ $product->pivot->price }}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>{{ $order->total_price }}</td>
                        <td>{{ $order->total_price_with_iva }}</td>
                        <td>{{ $order->order_date }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <a class="btn btn-primary" href={{route("orders.create")}}>
                Formulari per inserció de Comandes
            </a>
        </div>
    </div>
</x-app-layout>

<script defer src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXlY3/3vzpFfgccz7zU3YLQjSxsb6N0IFB+z7rrn3A88mTAA8bT8b4O2L8w4" crossorigin="anonymous"></script>
<script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-cVKIPhG8c3AnjE7e3hf9gzUqvQhtP6HfhZfp26d5URjo8U2tE3JfCFk3PB6h59ni" crossorigin="anonymous"></script>
</body>
</html>
