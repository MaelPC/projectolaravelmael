<?php

namespace App\Http\Controllers;

use App\Models\Discount;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class DiscountController extends Controller
{
    public function all(){
        $discounts = Discount::all();
        return view('alldiscounts')->with('discounts', $discounts);
    }
    public function delete(Discount $discount):RedirectResponse{
        $discount->delete();
        return Redirect::route('discounts.all');
    }

    public function deleteconfirmation(Discount $discount){
        return view('deleteconfirmationdiscount')->with('discount', $discount);
    }

    public function insert(Request $request):RedirectResponse{

        $validated = $request->validate([
            'name' => 'required|unique:discounts|max:100',
            'discount' => 'required'
        ]);

        DB::table('discounts')->insert(['name'=>$validated['name'], 'discount'=>$validated['discount']]);

        return Redirect::route('discounts.all');
    }

    public function editdiscount(Discount $discount)
    {
        return view('editdiscount', compact('discount'));
    }

    public function updatediscount(Request $request, Discount $discount): RedirectResponse
    {
        $validated = $request->validate([
            'name' => 'required|max:100|unique:discounts,name,' . $discount->id,
            'discount' => 'required|numeric|min:0',
        ]);

        Product::where('discount_id', $discount->id)->update(['discount_name' => $validated['name']]);

        $discount->update($validated);
        return Redirect::route('discounts.all');
    }

}
