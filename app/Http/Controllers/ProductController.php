<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Discount;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use PhpParser\Node\Expr\PreDec;

class ProductController extends Controller
{
    public function all(){
        $products = Product::all();
        return view('allproducts')->with('products', $products);
    }
    public function delete(Product $product):RedirectResponse{
        $product->delete();
        return Redirect::route('products.all');
    }

    public function deleteconfirmation(Product $product){
        return view('deleteconfirmation')->with('product', $product);
    }

    public function insert(Request $request):RedirectResponse{
        $validated = $request->validate([
            'name' => 'required|unique:products|max:100',
            'quantity' => 'required',
            'price' => 'required',
            'description'=>'max:115',
            'category_id'=>'required|exists:categories,id',
            'discount_id'=>'required|exists:discounts,id',
        ]);

        $categoryModel = Category::find($request->category_id);
        $name = $categoryModel->name;
        $discountModel = Discount::find($request->discount_id);
        $dname = $discountModel->name;

        DB::table('products')->insert(['name'=>$validated['name'], 'quantity'=>$validated['quantity'], 'price'=>$validated['price'],'description'=>$validated['description']."",'category_id'=>$validated['category_id'],'category_name'=>$name,'discount_id'=>$validated['discount_id'],'discount_name'=>$dname]);

        return Redirect::route('products.all');
    }
}
