<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Client;
use App\Models\Discount;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function all()
    {
        $orders = Order::with('client','products')->get();
        return view('allorders', compact('orders'));
    }public function create()
    {
        $clients = Client::all();
        $products = Product::all();
        return view('createOrder', compact('clients', 'products'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'client_id' => 'required|exists:clients,id',
            'order_date' => 'required|date',
            'products.*.id' => 'required|exists:products,id',
            'products.*.quantity' => 'required|integer|min:1',
        ]);

        foreach ($request->products as $producte) {

            $productModel = Product::find($producte['id']);
            $quantity = $producte['quantity'];
            if (($productModel->quantity - $quantity) < 0) {
                return redirect()->back()->with('quantity', 'La quantitat sol·licitada per ' . $productModel->name . ' excedeix l\'stock disponible.');
            }
        }

        $order = Order::create([
            'client_id' => $request->client_id,
            'order_date' => $request->order_date,
        ]);

        $discountPercentage=0;
        $order->save();
        $totalWithIva = 0;
        $totalWithoutIva = 0;
        foreach ($request->products as $product) {



            $productModel = Product::find($product['id']);
            $category = Category::find($productModel->category_id);
            $discount = Discount::find($productModel->discount_id);
            $quantity = $product['quantity'];

            $price = $productModel->price;
            $ivaPercentage = $category->iva;
            $discountPercentage = $discount->discount;

            $totalProductWithoutIva = ($price * $quantity) * (1 - ($discountPercentage / 100));
            $totalWithoutIva += $totalProductWithoutIva;
            $totalProductWithIva = $totalProductWithoutIva * (1 + ($ivaPercentage / 100));
            $totalWithIva += $totalProductWithIva;

            $order->products()->attach($product['id'], [
                'quantity' => $quantity,
                'price' => $price,
            ]);

            $productModel->quantity -= $quantity;
            $productModel->save();

            $order->total_price = $totalWithoutIva;
            $order->total_price_with_iva = $totalWithIva;
            $order->save();
        }

        return redirect()->route('orders.all')->with('success', 'Comanda creada amb èxit');
    }
}
