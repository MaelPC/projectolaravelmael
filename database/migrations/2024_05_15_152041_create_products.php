<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('quantity')->default(1);
            $table->integer('price');
            $table->string('description')->default('');
            $table->foreignId('category_id')->default(1)->constrained()->onDelete('set default');
            $table->string('category_name')->default('');
            $table->foreignId('discount_id')->default(1)->constrained()->onDelete('set default');
            $table->string('discount_name')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
