<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class products extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('products')->insert(['name'=>'Test', 'quantity'=>1, 'price'=>33,'description'=>'TEST DESC','category_id'=>1,'category_name'=>'Primera necesitat','discount_id'=>1,'discount_name'=>'Res']);
        DB::table('products')->insert(['name'=>'Test1', 'quantity'=>1, 'price'=>33,'description'=>'TEST DESC1','category_id'=>2,'category_name'=>'Utensilis','discount_id'=>2,'discount_name'=>'10% menys!']);
        DB::table('products')->insert(['name'=>'Test2', 'quantity'=>1, 'price'=>33,'description'=>'TEST DESC2','category_id'=>3,'category_name'=>'Tecnologia','discount_id'=>3,'discount_name'=>'1 quart menys del preu habitual!']);
        DB::table('products')->insert(['name'=>'Test3', 'quantity'=>1, 'price'=>33,'description'=>'TEST DESC3','category_id'=>4,'category_name'=>'Electronica','discount_id'=>4,'discount_name'=>'A meitat de preu!']);
        DB::table('products')->insert(['name'=>'Test4', 'quantity'=>1, 'price'=>33,'description'=>'TEST DESC4','category_id'=>5,'category_name'=>'Minerales','discount_id'=>5,'discount_name'=>'Liquidació, un 75% menys del preu normal!']);
    }
}
