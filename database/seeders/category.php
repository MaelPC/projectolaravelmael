<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class category extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('categories')->insert(['name'=>'Primera necesitat', 'iva'=>7]);
        DB::table('categories')->insert(['name'=>'Utensilis', 'iva'=>10]);
        DB::table('categories')->insert(['name'=>'Tecnologia', 'iva'=>12]);
        DB::table('categories')->insert(['name'=>'Electronica', 'iva'=>15]);
        DB::table('categories')->insert(['name'=>'Minerales', 'iva'=>21]);

    }
}
