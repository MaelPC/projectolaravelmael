<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(discounts::class);
        $this->call(category::class);
        $this->call(products::class);
        $this->call(clients::class);
        $this->call(orders::class);
    }
}
