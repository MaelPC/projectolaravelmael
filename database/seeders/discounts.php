<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class discounts extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('discounts')->insert(['name'=>'Res', 'discount'=>0]);
        DB::table('discounts')->insert(['name'=>'10% menys!', 'discount'=>10]);
        DB::table('discounts')->insert(['name'=>'1 quart menys del preu habitual!', 'discount'=>25]);
        DB::table('discounts')->insert(['name'=>'A meitat de preu!', 'discount'=>50]);
        DB::table('discounts')->insert(['name'=>'Liquidació, un 75% menys del preu normal!', 'discount'=>75]);
    }
}
