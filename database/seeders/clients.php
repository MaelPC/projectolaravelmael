<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class clients extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('clients')->insert(['name'=>'Test', 'email'=>"Test@Test.test", 'postalcode'=>"08005",'phonenumber'=>'+34 555 777 115', 'address'=>'Carrer Random 33']);
        DB::table('clients')->insert(['name'=>'Test2', 'surname'=>'Testapellido', 'email'=>"Test2@Test.test", 'postalcode'=>"08005",'phonenumber'=>'+34 555 777 115', 'address'=>'Carrer Random 33']);
        DB::table('clients')->insert(['name'=>'Test3', 'email'=>"Test3@Test.test", 'postalcode'=>"08005",'phonenumber'=>'+34 555 777 115', 'address'=>'Carrer Random 33']);
        DB::table('clients')->insert(['name'=>'Test4', 'surname'=>'Testapellido', 'email'=>"Test4@Test.test", 'postalcode'=>"08005",'phonenumber'=>'+34 555 777 115', 'address'=>'Carrer Random 33']);
        DB::table('clients')->insert(['name'=>'Test5', 'email'=>"Test5@Test.test", 'postalcode'=>"08005",'phonenumber'=>'+34 555 777 115', 'address'=>'Carrer Random 33']);
        DB::table('clients')->insert(['name'=>'Test6', 'surname'=>'Testapellido', 'email'=>"Test6@Test.test", 'postalcode'=>"08005",'phonenumber'=>'+34 555 777 115', 'address'=>'Carrer Random 33']);
    }
}
